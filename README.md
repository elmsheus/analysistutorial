# analysistutorial

This is very simple analysis code based on the ATLAS beginners tutorial from https://atlassoftwaredocs.web.cern.ch/ABtutorial/. The code has been implemented here until a simple xAOD reading and printing Event and Run numbers at https://atlassoftwaredocs.web.cern.ch/ABtutorial/alg_basic_review/. 

The following instructions are a stripped down version of the very detailed instructions linke above.

* Code setup with:
```
ssh lxplus.cern.ch
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
lsetup git
mkdir mytutorial
cd mytutorial
git clone https://gitlab.cern.ch/elmsheus/analysistutorial.git
mkdir build run
```

* Compile it with AnalysisBase for EventLoop usage with:
```
asetup AnalysisBase,22.0.95
cd build
cmake ../analysistutorial/
make
source x86_64-centos7-gcc11-opt/setup.sh 
cd ../run
ATestRun_eljob.py --submission-dir=submitDir
```
AnalysisBase,22.0.95 uses ROOT 6.24/06


* Compile it with the Athena dev3LCG nightly for Athena usage with:
```
asetup Athena,master--dev3LCG,latest,gcc11
cd build
cmake ../analysistutorial/
make
source x86_64-centos7-gcc11-opt/setup.sh 
cd ../run
athena.py MyAnalysis/ATestRun_jobOptions.py
```
The Athena dev3LCG nightly uses the HEAD version of ROOT.

* Input samples:
By default it's using the xAOD files defined at https://atlassoftwaredocs.web.cern.ch/ABtutorial/xaod_samples/ through the environment variables in `analysistutorial/MyAnalysis/share/ATestRun_eljob.py` or `analysistutorial/MyAnalysis/share/ATestRun_jobOptions.py`. You can tweak these files to use different input xAODs.
